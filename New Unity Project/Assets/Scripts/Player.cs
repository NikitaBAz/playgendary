﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    [SerializeField] private TerrainerSpawner terrainerSpawner;
    [SerializeField] private Text scoreText;
   

    private Animator animator;
    private bool isJumpping;
    private int score;
    


    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    private void FixedUpdate()
    {
        score++;
    }
    void Update()
    {

        scoreText.text = "Score:" + score;  
        if (Input.GetKeyDown(KeyCode.W) && !isJumpping)
        {
            
            float newZ = 0;
            if (transform.position.z % 1 != 0)
            {
                newZ = transform.position.z - Mathf.Round(transform.position.z);
            }
            MoveCharacter(new Vector3(1, 0, newZ));

        }
        else if (Input.GetKeyDown(KeyCode.A) == !isJumpping)
        {

            MoveCharacter(new Vector3(0, 0, 1));
        }
        else if (Input.GetKeyDown(KeyCode.D) == !isJumpping)
        {

            MoveCharacter(new Vector3(0, 0, -1));
        }
    }

    private void MoveCharacter(Vector3 difference)
    {
        animator.SetTrigger("Jump");
        isJumpping = true;
        transform.position = (transform.position + difference);
        terrainerSpawner.SPawnTerrain(false, transform.position);
    }
    public void finishJump()
    {
        isJumpping = false;
    }
}
