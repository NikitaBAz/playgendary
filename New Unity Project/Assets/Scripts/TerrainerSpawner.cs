﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainerSpawner : MonoBehaviour
{
    [SerializeField] private int minDistsnceFromPlayer;
    [SerializeField] private int maxTerrainCount;
    [SerializeField] private List<TerrainData> terrainDatas = new List<TerrainData>();
    [SerializeField] private Transform terrainHolder;
   
    
    private List<GameObject> currentTerrains = new List<GameObject>();
    [HideInInspector]private Vector3 currentLoc = new Vector3(0, 0, 0);


    private void Start()
    {
        for (int i = 0; i < maxTerrainCount; i++)
        {
            SPawnTerrain(true, new Vector3(0,0,0));
        }
        maxTerrainCount = currentTerrains.Count; 
    }
    
    public void SPawnTerrain(bool isStart, Vector3 posPLayer)
    {
        if ((currentLoc.x - posPLayer.x < minDistsnceFromPlayer) || (isStart))
        {
            int whichTerrain = Random.Range(0, terrainDatas.Count);
            int terrrainInSuccession = Random.Range(1, terrainDatas[whichTerrain].maxInSuccesion);

            for (int i = 0; i < terrrainInSuccession; i++)
            {
                GameObject terrain = Instantiate(terrainDatas[whichTerrain].posibleterrain[Random.Range(0, terrainDatas[whichTerrain].posibleterrain.Count)], currentLoc, Quaternion.identity, terrainHolder);
                currentTerrains.Add(terrain);
                if (!isStart)
                {
                    if (currentTerrains.Count > maxTerrainCount)
                    {
                        Destroy(currentTerrains[0]);

                        currentTerrains.RemoveAt(0);
                    }
                }
                currentLoc.x++;
            } 
        }
        
          
    }
}
